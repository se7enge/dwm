/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int gappx     = 10;       /* gaps between windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int swallowfloating    = 0;        /* 1 means swallow floating windows by default */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const int horizpadbar        = 20;       /* horizontal padding for statusbar */
static const int vertpadbar         = 12;       /* vertical padding for statusbar */
static const char *fonts[]          = { "NotoSansMono Nerd Font:style=Medium:size=12:antialias=true:autohint=true",
										"Ubuntu Nerd Font:style=Regular:size=11:antialias=true:autohint=true"								
																	};
static const char dmenufont[]       = "NotoSansMono Nerd Font:style=Medium:size=12:antialias=true:autohint=true";
static unsigned int baralpha        = 0xf0;
static unsigned int borderalpha     = OPAQUE;
static const char col_gray1[]       = "#2a2e32";
static const char col_gray2[]       = "#444444";
static const char col_gray3[]       = "#a1a9b1";
static const char col_gray4[]       = "#c7c7d1"; /* options from lightest to darkest: #e7e7f2, #d7d7e2, #c7c7d1 */
static const char col_gray5[]       = "#e7e7f2"; /* options from lightest to darkest: #e7e7f2, #d7d7e2, #c7c7d1 */
static const char col_green1[]      = "#25c97d";
static const char col_green2[]      = "#1cdc9a";
static const char col_green3[]      = "#1abc9c";
static const char col_yellow[]      = "#f27835";
static const char col_cyan1[]       = "#448de0";
static const char col_cyan2[]       = "#25bfff";
static const char col_cyan3[]       = "#70a8e7";
static const char col_cyan4[]       = "#224670";
static const char col_purp[]        = "#a064d8"; // #8e44ad
static const char col_lay[]         = "#a1a9b1"; /* #abbae5 (dulled lilac), #a1a9b1 (dulled gray) */
static const char col_lilac[]       = "#718ad3";
static const char col_dulac[]       = "#abbae5";
static const char col_inac[]        = "#5b6268";
static const char col_inacbor[]     = "#43484d"; /* #484e53 */
static const char col_bluegray1[]   = "#707a96"; // original that 2 & 1 are based on
static const char col_bluegray2[]   = "#8d95ac"; // sel
static const char col_bluegray3[]   = "#596178"; // unsel
static const char *colors[][3]      = {
	/*                    fg          bg         border   */
	[SchemeNorm]      = { col_gray4,  col_gray1, col_bluegray3 },
	[SchemeSel]       = { col_green2, col_gray1, col_cyan1  },
	[SchemeLayout]    = { col_gray4,  col_gray1, col_gray2  },
	[SchemeTitleNorm] = { col_inac,   col_gray1, col_gray2  },
	[SchemeTitleSel]  = { col_lay,    col_gray1, col_cyan2  },
	[SchemeUrg]       = { col_yellow, col_gray1, col_cyan2  },
};

static const char *tagsel[][2] = {
   /*   fg         bg    */
  { col_bluegray2, col_gray1 }, /* norm */
  { col_green2,    col_gray1 }, /* sel */
  { col_cyan3,     col_gray1 }, /* occ but not sel */
  { col_cyan1,     col_gray1 }, /* has pinned tag */
};
/* tagging */
/*static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };*/
static const char *tags[] = { "dev", "www", "mus", "sys", "vir", "com", "etc" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class              instance     title           tags mask  isfloating  isterminal  noswallow  monitor */
	{ "Gimp",             NULL,        NULL,           0,         1,          0,           0,        -1 },
	{ "Firefox",          NULL,        NULL,           1 << 8,    0,          0,          -1,        -1 },
	{ "st-256color",      NULL,        NULL,           0,         0,          1,           0,        -1 },
	{ NULL,               NULL,        "Event Tester", 0,         0,          0,           1,        -1 }, /* xev */
};

/* layout(s) */
static const float mfact     = 0.50; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },

	/* alternative symbols for layouts */	
	//{ " ",      tile },    /* first entry is default */
	//{ "",      tile },    /* first entry is default */
	//{ "",      NULL },    /* no layout function means floating behavior */
	//{ "",      NULL },    /* no layout function means floating behavior */
	//{ """,      NULL },    /* no layout function means floating behavior */
	//{ " ",      NULL },    /* no layout function means floating behavior */
	//{ "",      monocle },
	//{ " ",      monocle },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-i", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray4, "-sb", col_cyan1, "-sf", col_gray1, NULL };
static const char *termcmd[]  = { "st", NULL };
static const char *termdircmd[]  = { "termnewdir.sh", NULL };
static const char *roficmd[]  = { "rofi", "-show", "drun", NULL };
static const char *lockcmd[]  = { "betterlockscreen", "-l", NULL };
static const char *playpausecmd[]  = { "playerctl", "-p", "spotify", "play-pause", NULL };
static const char *prevcmd[]  = { "playerctl", "-p", "spotify", "previous", NULL };
static const char *nextcmd[]  = { "playerctl", "-p", "spotify", "next", NULL };
static const char *seekbackcmd[]  = { "playerctl", "-p", "spotify", "position", "10-", NULL };
static const char *seekforcmd[]  = { "playerctl", "-p", "spotify", "position", "10+", NULL };
static const char *scrnshtcmd[]  = { "scrot", "-s", "/home/se7enge/Pictures/Screensnaps/$%Y-%m-%d_%H-%M-%S.png", NULL };
static const char *scrncapcmd[]  = { "scrot", "-u", "/home/se7enge/Pictures/Screensnaps/$%Y-%m-%d_%H-%M-%S.png", NULL };

#include "movestack.c"
static const Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } },
	{ MODKEY,                       XK_r,      spawn,          {.v = roficmd } },
	{ MODKEY,                       XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY|ShiftMask,             XK_Return, spawn,          {.v = termdircmd } },
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY|ShiftMask,             XK_j,      movestack,      {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_k,      movestack,      {.i = -1 } },
	{ MODKEY|ControlMask,           XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY|ShiftMask,             XK_c,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
//	{ MODKEY,                       XK_minus,  setgaps,        {.i = -1 } },
//	{ MODKEY,                       XK_equal,  setgaps,        {.i = +1 } },
//	{ MODKEY|ShiftMask,             XK_equal,  setgaps,        {.i = 0  } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
/*	TAGKEYS(                        XK_8,                      7)*/
/*	TAGKEYS(                        XK_9,                      8)*/
	{ MODKEY|ShiftMask,             XK_Print,  spawn,          {.v = scrncapcmd } },
	{ MODKEY,                       XK_Print,  spawn,          {.v = scrnshtcmd } },
	{ MODKEY,                       XK_equal,  spawn,          {.v = seekforcmd } },
	{ MODKEY,                       XK_minus,  spawn,          {.v = seekbackcmd } },
	{ MODKEY,                       XK_F12,    spawn,          {.v = nextcmd } },
	{ MODKEY,                       XK_F11,    spawn,          {.v = prevcmd } },
	{ MODKEY,                       XK_F10,    spawn,          {.v = playpausecmd } },
	{ MODKEY,                       XK_x,      spawn,          {.v = lockcmd } },
	{ MODKEY,             			XK_F5,      quit,          {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static const Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

