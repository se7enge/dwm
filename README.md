# A Custom 'dwm' Build

![desktop_screenshot](./previews/dwm_desktop1.png)

## About
[dwm](https://dwm.suckless.org/) is a fast, minimal, no-nonsense dynamic tiling window manager, written entirely in C, for the X Window System for Unix-like operating systems; developed by [suckless](https://suckless.org/philosophy/).

Some may consider dwm to be "too minimal" out the box and therefore somewhat unusable for their needs. This is my personal build/fork of dwm which seeks to address that "issue" without adding too much superfluous code. It has some key community patches - as well as additional personal changes to the source code - pre-applied for saner defaults and greater usability.

### Patches included in this build:
+ attachbottom (preserves position of pre-existing clients whilst new clients are added to the bottom of the stack)
+ alwayscenter (always centres new floating clients)
+ fullgaps (adds full featured and customisable gaps around clients)
+ movestack (allows individual clients to be moved through the stack)
+ pertag (allows each tag to have layouts assigned to it independently)
+ preserverestart (forces clients to remain on respective tags upon restarting dwm)
+ statuspadding (adds configurable additional horizontal/vertical padding to the status bar)
+ alttagsdecoration (changes occupied tag decoration to `<tag>`)
+ swallow (allows window swallowing when launching graphical programs from the terminal)

### Additional personal changes:
+ Added additional colour scheme options to allow for a greater number of elements of the status bar to be themed independently of one another.
+ Changed the behaviour of the active window title highlighting function to automatically truncate to the length of the current title.
+ Changed the behaviour of window title drawing generally to always truncate titles (with an ellipsis) that are greater than 36 characters.
+ Added gaps to the monocle layout for a more consistent look and feel across layouts.

### Additional considerations:
+ Automatically starting applications can be handled by an `.xinitrc` file and I have therefore elected not to include the 'autostart' patch.
+ Restarting dwm in place can be achieved by adding a while loop into one's `.xinitrc` and I have therefore elected not to include the 'restartsig' patch.

## Dependencies
+ nerd-fonts-noto-sans-mono
+ ttf-ubuntu-nerd
+ dmenu
+ slstatus (optional)
+ st (optional)

## Installation
```
$ git clone https://gitlab.com/se7enge/dwm.git
$ cd dwm
$ sudo make clean install
```

## Keybindings
See `config.h` or `config.def.h`.
